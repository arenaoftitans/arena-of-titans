export default {
    "actions": {
        "dicarded_card": "{{playerName}} just discarded a card",
        "passed_turn": "{{playerName}} just passed his/her turn",
        "played_card": "{{playerName}} just played a card",
        "played_special_action": "{{playerName}} just played a special action on {{targetName}}",
        "played_trump": "{{playerName}} just played a trump on {{targetName}}",
        "problem": "A problem occured",
        "special_action_assassination": "Click on the pawn of a player. You will then be able to move it back.",
        "special_action_info_popup": "You just played a card with a special action {{action}}",
        "trump_played_by": "Played by {{initiator}}"
    },
    "cards": {
        "assassin": "Move two squares in line or diagonal.",
        "assassin_black": "Black Assassin",
        "assassin_blue": "Blue Assassin",
        "assassin_complementary_description": "Special action: Assassination to make a player move back.",
        "assassin_red": "Red Assassin",
        "assassin_yellow": "Yellow Assassin",
        "bishop": "Move two squares in diagonal. Can move on two different colors.",
        "bishop_black": "Black Bishop",
        "bishop_blue": "Blue Bishop",
        "bishop_red": "Red Bishop",
        "bishop_yellow": "Yellow Bishop",
        "king": "Move three squares in line.",
        "king_black": "Black King",
        "king_blue": "Blue King",
        "king_red": "Red King",
        "king_yellow": "Yellow King",
        "knight": "Move one square in L.",
        "knight_black": "Black Knight",
        "knight_blue": "Blue Knight",
        "knight_red": "Red Knight",
        "knight_yellow": "Yellow Knight",
        "queen": "Move two squares in line or diagonal.",
        "queen_black": "Black Queen",
        "queen_blue": "Blue Queen",
        "queen_red": "Red Queen",
        "queen_yellow": "Yellow Queen",
        "warrior": "Move one square in line.",
        "warrior_black": "Black Warrior",
        "warrior_blue": "Blue Warrior",
        "warrior_red": "Red Warrior",
        "warrior_yellow": "Yellow Warrior",
        "wizard": "Move one squares in line or diagonal. Can move on a square of any color.",
        "wizard_black": "Black Wizard",
        "wizard_blue": "Blue Wizard",
        "wizard_red": "Red Wizard",
        "wizard_yellow": "Yellow Wizard"
    },
    "game": {
        "connection_lost": "Lost connection with the server",
        "create": {
            "AI": "Computer",
            "CLOSED": "Closed",
            "OPEN": "Open",
            "TAKEN": "Taken",
            "add": "Add Slot",
            "create": "Create the game",
            "enter_name": "Enter your name:",
            "heroes": "Heroes",
            "invite": "Invite",
            "invite_text": "To join this game, share this link:",
            "others": "Other Players",
            "popup_select_avatar": "Select your avatar",
            "slot": "Slot"
        },
        "game_over": "Game Over",
        "play": {
            "back_home_popup_title": "What do you want to do?",
            "cancel_special_action": "Skip special action",
            "complete_turn": "Complete the turn",
            "complete_turn_confirm_message": "Are you sure you want to complete your turn?",
            "discard": "Discard",
            "discard_confirm_message": "Are you sure you want to discard {{cardName}}?",
            "discard_no_selected_card": "You must select a card",
            "game_over": "You just completed the game! Your rank:",
            "no_action": "No action yet",
            "pass": "Pass",
            "pass_confirm_message": "Are you sure you want to pass your turn?",
            "player_box_cards": "Last played cards",
            "player_box_trumps": "Trumps active on player",
            "select_trump_target": "Who should be the target of {{trumpname}}?",
            "whose_turn_message": "It is the turn of <br /><strong class=\"blue-text\">{{playerName}}</strong>",
            "your_turn": "It's your turn!"
        },
        "visit": {
            "cards": "To do this, you need to use the cards. Click on one and the squares on which you can move will be hightlighted.",
            "goal": "The goal of the game is to go to the last line as fast as you can and stay there for a turn.",
            "intro": "You look like a newcomer. Let me help you.",
            "notifications": "The last played actions are visible here.",
            "propose": "Do you want to see the tutorial?",
            "title": "Tutorial",
            "trumps": "You can also use trumps to get bonuses or hinder your oponents."
        }
    },
    "global": {
        "alias": "Alias",
        "back_home": "Home page",
        "cancel": "Cancel",
        "cannot_do_action": "You cannot perform this action:",
        "create_new_game": "New Game",
        "edit": "Edit",
        "name": "Name:\u00a0{{playerName}}",
        "no": "No",
        "ok": "OK",
        "propose_in_game_help_option": "View in game help",
        "propose_tutorial_option": "Tutorial",
        "save": "Save",
        "sound_option": "Sound",
        "yes": "Yes"
    },
    "heroes": {
        "araneen": "(en) Ulya fait partie des Yrilles, les chevaucheuses d\u2019\u00e9lite aux ordres de la reine aran\u00e9enne. Accompagn\u00e9e de son Arakoss V\u00e9nisse, Ulya s\u2019est distingu\u00e9e par de nombreux exploits en tournoi et en terrassant un dragon des sables. Les tatouages qui couvrent le corps de l\u2019aran\u00e9enne illustrent ses prouesses au combat. Le duo ne conna\u00eet \u00e0 ce jour aucune d\u00e9faite.",
        "araneen_power": "(en) \u00ab\u00a0Domination\u00a0\u00bb (passif)\u00a0: Lorsque vous jouez une carte \u00ab\u00a0Dame\u00a0\u00bb vous pouvez vous d\u00e9placer de 3 cases au lieu de 2.",
        "centaur": "(en) Comme la plupart des Centaures, Garez appr\u00e9cie la boisson, les grillades et les femmes. Lorsqu\u2019il n\u2019est pas occup\u00e9 \u00e0 festoyer, il endosse le r\u00f4le de commandant des arm\u00e9es du seigneur de guerre Rennack. N\u2019appr\u00e9ciant pas particuli\u00e8rement le combat, il \u00e9limine ses ennemis en un temps record. Ceux qui se trouvent sur sa route peuvent s\u2019attendre \u00e0 une mort aussi rapide qu\u2019impr\u00e9visible.",
        "centaur_power": "(en) \u00ab\u00a0Chevauch\u00e9e Intr\u00e9pide\u00a0\u00bb (passif)\u00a0: Les d\u00e9placements des \u00ab\u00a0Cavaliers\u00a0\u00bb ne peuvent \u00eatre bloqu\u00e9s par aucun effet d\u2019atouts ou de comp\u00e9tence de h\u00e9ros.",
        "daemon": "(en) Kharliass est un d\u00e9mon de classe majeure connue pour avoir sem\u00e9 la panique dans divers pays du monde des humains et orchestr\u00e9 un certains nombres d\u2019\u00e9meutes particuli\u00e8rement sanglantes durant l\u2019Antiquit\u00e9. Elle plonge ses proies dans un \u00e9tat de confusion totale en rev\u00eatant leur propre apparence juste avant de les tuer.",
        "daemon_power": "(en) \u00ab\u00a0M\u00e9tamorphose\u00bb (actif)\u00a0: Pendant 1 tour vous pouvez prendre l\u2019apparence d\u2019un de vos adversaires. Cette comp\u00e9tence est alors remplac\u00e9e par la comp\u00e9tence h\u00e9ros du joueur s\u00e9lectionn\u00e9 et son co\u00fbt d\u2019utilisation est nul.",
        "dwarf": "(en) Dj\u00f6r est un b\u00e2tisseur l\u00e9gendaire dont la renomm\u00e9e d\u00e9passe de loin les fronti\u00e8res de Nifelheim, le royaume des nains. On raconte qu\u2019il aurait fait fortune en \u00e9rigeant le ch\u00e2teau du roi Rogn\u00efr et que depuis lors il parcourt les terres du Miroir \u00e0 la recherche d\u2019aventures et de nouveaux d\u00e9fis.",
        "dwarf_power": "(en) \u00ab\u00a0Infranchissable\u00a0\u00bb (passif)\u00a0: Vos atouts \u00ab\u00a0Forteresses\u00a0\u00bb ne peuvent \u00eatre annul\u00e9s par l\u2019effet de l\u2019atout \u00ab\u00a0B\u00e9lier\u00a0\u00bb",
        "elf": "(en) Chasseuse hors paire, Arline est une Elfe issue d\u2019une famille modeste de forgerons. Elle a apprit \u00e0 manier l\u2019arc d\u00e8s son plus jeune \u00e2ge et s\u2019est b\u00e2tie une solide r\u00e9putation d\u2019arch\u00e8re d\u2019\u00e9lite m\u00eame parmi les hautes castes de la soci\u00e9t\u00e9 elfique. Ceux qui l\u2019ont sous-estim\u00e9e ne sont plus en mesure de le regretter.",
        "elf_power": "(en) \u00ab Brume Nocturne\u00bb (actif) : Lorsque vous d\u00e9clenchez cette comp\u00e9tence, vous disparaissez du plateau de jeu et vos adversaires ne peuvent plus vous cibler pendant 1 tour.",
        "orc": "(en) Razbrak est le fr\u00e8re cadet du chef du clan Akta-Ross. Expert en maniement des armes, il affectionne en particulier la hache dont il se sert pour d\u00e9capiter ses ennemis. Sous ses airs de brute sauvage et sanguinaire se cache un v\u00e9ritable artiste\u00a0: il collectionne les cr\u00e2nes de ses victimes afin de les sculpter et d\u2019en faire de magnifiques bougeoirs.",
        "orc_power": "(en) \u00ab\u00a0Force de la Nature\u00a0\u00bb (passif)\u00a0: Les atouts \u00ab\u00a0Tour\u00a0\u00bb ne vous affectent pas.",
        "reaper": "(en) Consid\u00e9r\u00e9 comme l\u2019un des mages les plus puissants de sa g\u00e9n\u00e9ration, Mir\u00efndrel s\u2019est illustr\u00e9 lors des deux derni\u00e8res grandes guerres contre les d\u00e9mons en d\u00e9ployant des stratag\u00e8mes particuli\u00e8rement ing\u00e9nieux. Il utilise le terrain qui l\u2019entoure pour pi\u00e9ger ses adversaires ou s\u2019offrir un avantage concurrentiel significatif.",
        "reaper_power": "(en) \u00ab Terraformage \u00bb (actif) : Lorsque cette comp\u00e9tence est activ\u00e9e, vous avez la possibilit\u00e9 de changer la couleur de n\u2019importe quelle case du plateau de jeu. Le changement de couleur est permanent.",
        "thief": "(en) Luni est une jeune Ombre aux faits d\u2019armes encore inconnus. Elle ma\u00eetrise aussi bien l\u2019art des runes que celui du combat rapproch\u00e9 et son agilit\u00e9 exceptionnelle fait d\u2019elle un adversaire redoutable. Apr\u00e8s avoir obtenu son dipl\u00f4me de la Zefo, elle s\u2019est engag\u00e9e dans la division des renseignements au service du roi des Ombres.",
        "thief_power": "(en) \u00ab\u00a0Lame secr\u00e8te\u00a0\u00bb (actif)\u00a0: Lorsque cette comp\u00e9tence est activ\u00e9e, vous avez la possibilit\u00e9 de d\u00e9fausser une carte \u00ab\u00a0Fou\u00a0\u00bb pour faire reculer l\u2019adversaire de votre choix d\u2019une case de l\u2019une des deux couleurs du \u00ab\u00a0Fou\u00a0\u00bb d\u00e9fauss\u00e9."
    },
    "site": {
        "connection_button": "Connect",
        "connection_game": "Play",
        "connection_label": "Connection",
        "header_community": "Community",
        "header_news": "News",
        "header_play": "Play",
        "header_ranking": "Ranking",
        "header_rules": "Rules",
        "header_synopsis": "Synopsis",
        "homepage": {
            "families": "Four families control the Arena, use the influence of their members in order to cross the different areas !",
            "heroes": "Choose your Hero among the 8 people and unleash his power against your opponents !",
            "pitch": "<strong>Will you dare enter in the Arena of Titans?</strong><br>                 Create your path with the movements cards,<br>                 Use those of your oponents to move faster,<br>                 Block them with your trumps,<br>                 Arrive fist\u2026<br>                 And survive!",
            "slider1": "A unique gameplay",
            "slider2": "Challenge up to 7 of your friends",
            "slider3": "Beta Version is available !",
            "tutorial": "<strong>Tutorial</strong>"
        },
        "rules": {
            "heroes": "Choisissez votre H\u00e9ros parmi les 8 peuples.<br/>\n                Chaque H\u00e9ros poss\u00e8de une comp\u00e9tence sp\u00e9cifique qui vient compl\u00e9ter votre Build.",
            "moves": "<h2>D\u00e9placements</h2>\n                <p>Les cartes D\u00e9placement sont les cartes \u00e0 bords jaunes, noirs, rouges et bleus.<br/>\n                    Lorsque vous utilisez une carte D\u00e9placement, vous pouvez avancer votre pion sur le plateau.<br/>\n                    Le nombre de cases, la couleur des cases et le type de mouvement que vous pouvez faire sont impos\u00e9s par la carte que vous avez jou\u00e9e.\n                </p>\n                <img src=\"/latest/assets/rules/aot-move.png\"\n                     alt=\"The moves of the game\" />",
            "rules": "<h1>R\u00e8gles G\u00e9n\u00e9rales</h1>\n            <div class=\"half-column-left\">\n                <h2>But du jeu</h2>\n                <p>En partant d'un bout d'une branche du plateau, soyez le premier \u00e0 le traverser jusqu'\u00e0 atteindre la derni\u00e8re ligne oppos\u00e9e et \u00e0 y rester pendant un tour.</p>\n            </div>\n            <img class=\"half-column-right border-black\"\n                 src=\"/latest/assets/homepage/board.png\"\n                 alt=\"The board of the game\" />",
            "trumps": "<h2>Atouts</h2>\n                <p>Avant une partie, vous avez la possibilit\u00e9 de cr\u00e9er un Build contenant 4 Atouts.<br/>\n                    Les Atouts servent \u00e0 bloquer les autres joueurs, bloquer les atouts des autres joueurs, am\u00e9liorer vos d\u00e9placements,\u2026</p>\n                <img src=\"/latest/assets/rules/trumps.png\"\n                     alt=\"The trumps\" />"
        },
        "slide": {
            "elf": "Nul ne peut arr\u00eater ce qui est invisible",
            "orc": "Plus facile d'\u00eatre premier lorsqu'il ne reste plus d'adversaires"
        },
        "synopsis": {
            "pitch": "<h1>Synopsis</h1>\n                <p class=\"big-column-center\">Au commencement de tout, lorsque le monde n\u2019\u00e9tait que n\u00e9ant, quatre \u00eatres \u00e9merg\u00e8rent de la matrice originelle que les elfes nomment A\u00f6ctra : l\u2019essence de toutes les essences. Ces cr\u00e9atures furent appel\u00e9es les Titans. Deux femmes et deux hommes qui avaient pour noms Th\u00e9zalia, Noya, Da\u00eflum, et Kranth.</p>",
            "story": "<p>Les Titans cr\u00e9\u00e8rent les continents et les mers, les montagnes et les for\u00eats. Ils donn\u00e8rent ensuite la vie aux sept peuples que sont les Ombres, les Elfes, les Orcs, les Nains, les Arann\u00e9ens, les Centaures  et les Elvains. Ils furent rejoins plus tard par les D\u00e9mons qui constitu\u00e8rent le huiti\u00e8me peuple.</p>\n                    <p>Les peuples commenc\u00e8rent \u00e0 se battre pour obtenir le pouvoir de gouverner les diff\u00e9rents territoires, et avant de dispara\u00eetre, les Titans cr\u00e9\u00e8rent une immense ar\u00e8ne afin de les d\u00e9partager.  Chaque Titan cr\u00e9a une famille \u00e0 son effigie et lui assigna le contr\u00f4le de plusieurs zones de l\u2019Ar\u00e8ne.</p>\n                    <p>Depuis ce jour, chaque peuple envoie ses meilleurs combattants relever le d\u00e9fi de l\u2019Ar\u00e8ne des Titans : en utilisant l\u2019influence des quatre familles et les pouvoirs octroy\u00e9s par les Titans, les participants doivent r\u00e9ussir \u00e0 traverser l\u2019Ar\u00e8ne\u2026avant les autres concurrents !</p>\n                    <p>Le pouvoir ultime se m\u00e9rite.</p>"
        }
    },
    "trumps": {
        "assassination": "Assassination",
        "assassination_description": "Make an other player move back",
        "max_number_played_trumps": "You cannot play more trumps during this turn",
        "max_number_trumps": "This player cannot be the target of anymore trump during this turn",
        "reinforcements": "Reinforcements",
        "reinforcements_description": "Allow the player to play one more move.",
        "tower_black": "Black Tower",
        "tower_black_description": "Prevent the player to move on black squares.",
        "tower_blue": "Blue Tower",
        "tower_blue_description": "Prevent the player to move on blue squares.",
        "tower_red": "Red Tower",
        "tower_red_description": "Prevent the player to move on red squares.",
        "tower_yellow": "Yellow Tower",
        "tower_yellow_description": "Prevent the player to move on yellow squares."
    }
};